package main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.Calendar;

import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import tabs.*;

public class BudgetSheet extends JFrame implements ActionListener{

	private static final long serialVersionUID = -9207614727769190721L;
	
	private JLabel tithing;
	private MainTab main;
	private IncomeTab income;
	private ExpensesTab expenses;
	private ScheduleTab schedule;

	public static void main(String[] args){
		new BudgetSheet();
	}
	
	public BudgetSheet(){
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Budget Sheet");
		this.setIconImage(Toolkit.getDefaultToolkit().getImage("icon.png"));
		this.setPreferredSize(new Dimension(800, 500));
		init();
		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	
	public void init(){
		this.setLayout(new BorderLayout());
		
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		int min = 2017;
		String[] values = new String[currentYear - min + 1];
		for(int i = 0; i < (currentYear - min + 1); ++i){
			values[i] = Integer.toString(currentYear - i);
		}
		JComboBox<String> combo = new JComboBox<String>(values);
		combo.addActionListener(this);
		
		JLabel lastUpdated = new JLabel("Last Updated: " + Model.getInstance().getLastUpdated());
		
		double tithe = Model.getInstance().getTithing(currentYear);
		DecimalFormat df = new DecimalFormat("0.00");
		tithing = new JLabel("Tithing to be paid: $" + df.format(tithe));
		
		JLabel selectYear = new JLabel("Year:");
		JPanel top = new JPanel(new FlowLayout(FlowLayout.LEFT));
		top.add(selectYear);
		top.add(combo);
		top.add(Box.createRigidArea(new Dimension(50, 0)));
		top.add(lastUpdated);
		top.add(Box.createRigidArea(new Dimension(50, 0)));
		top.add(tithing);
		
		JTabbedPane tabs = new JTabbedPane();
		main = new MainTab();
		income = new IncomeTab();
		expenses = new ExpensesTab();
		schedule = new ScheduleTab();
		JScrollPane mainScroll = new JScrollPane(main);
		JScrollPane incomeScroll = new JScrollPane(income);
		JScrollPane expensesScroll = new JScrollPane(expenses);
		JScrollPane scheduleScroll = new JScrollPane(schedule);
		
		mainScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		mainScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		mainScroll.getHorizontalScrollBar().setUnitIncrement(20);
		mainScroll.getVerticalScrollBar().setUnitIncrement(20);
		incomeScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		incomeScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		incomeScroll.getHorizontalScrollBar().setUnitIncrement(20);
		incomeScroll.getVerticalScrollBar().setUnitIncrement(20);
		expensesScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		expensesScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		expensesScroll.getHorizontalScrollBar().setUnitIncrement(20);
		expensesScroll.getVerticalScrollBar().setUnitIncrement(20);
		scheduleScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scheduleScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scheduleScroll.getHorizontalScrollBar().setUnitIncrement(20);
		scheduleScroll.getVerticalScrollBar().setUnitIncrement(20);
		
		tabs.addTab("Main", mainScroll);
		tabs.addTab("Income", incomeScroll);
		tabs.addTab("Expenses", expensesScroll);
		tabs.addTab("Schedule", scheduleScroll);
		
		this.add(top, BorderLayout.NORTH);
		this.add(tabs, BorderLayout.CENTER);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void actionPerformed(ActionEvent e) {
		JComboBox<String> box = (JComboBox<String>) e.getSource();
		int year = Integer.valueOf((String)box.getSelectedItem());
		
		double tithe = Model.getInstance().getTithing(year);
		DecimalFormat df = new DecimalFormat("0.00");
		tithing.setText("Tithing to be paid: $" + df.format(tithe));
		main.setYear(year);
		income.setYear(year);
		expenses.setYear(year);	
		schedule.setYear(year);
		tithing.revalidate();
		tithing.repaint();
	}
}

package main;

import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import definitions.Account;
import definitions.Category;
import definitions.Expense;
import definitions.Income;
import definitions.PriorityQueue;
import definitions.ScheduledExpense;
import exceptions.AccountException;
import exceptions.CategoryException;

public class Model {

	private static Model instance = null;
	private static final int DAYS_BEFORE = 5;
	private static final int PAYCHECK_FREQUENCY = 2;
	
	public static Model getInstance(){
		if(instance == null){
			instance = new Model();
		}
		return instance;
	}
	
	private long lastUpdated;
	private long paycheck;
	private List<Expense> expenses;
	private List<Income> income;
	private List<ScheduledExpense> schedule;
	private Map<Category, Double> budget;
	private Map<Account, Double> balance;
	
	private Model(){
		paycheck = 0;
		expenses = new ArrayList<Expense>();
		income = new ArrayList<Income>();
		schedule = new ArrayList<ScheduledExpense>();
		budget = new HashMap<Category, Double>();
		balance = new HashMap<Account, Double>();
		readItems();
	}
	
	private void readItems(){
		List<String> errors = readIncome();
		errors.addAll(readExpenses());
		errors.addAll(readBudget());
		errors.addAll(readBalance());
		errors.addAll(readLastUpdated());
		errors.addAll(readSchedule());
		
		if(errors.size() > 0){
			JDialog dialog = new JDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setTitle("Errors");
			dialog.setPreferredSize(new Dimension(500, 500));
			dialog.pack();
			dialog.setLocationRelativeTo(null);
			dialog.setModal(true);
			
			JPanel pan = new JPanel();
			pan.setLayout(new BoxLayout(pan, BoxLayout.Y_AXIS));
			for(String str : errors){
				pan.add(new JLabel(str));
			}

			JScrollPane pane = new JScrollPane(pan);
			pane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			pane.getHorizontalScrollBar().setUnitIncrement(20);
			pane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
			pane.getVerticalScrollBar().setUnitIncrement(20);
			
			dialog.getContentPane().add(pane);
			
			dialog.addWindowListener(new WindowAdapter(){
				
				@Override
				public void windowClosed(WindowEvent e){ 
					System.exit(0);
				}
				
			});
			
			dialog.setVisible(true);
		}
	}
	
	private List<String> readIncome(){
		List<String> errors = new ArrayList<String>();
		try{
			File file = new File("/home/steven/Budget/income.csv");
			Scanner scanner = new Scanner(file);
			int i = 0;
			while(scanner.hasNextLine()){
				String line = scanner.nextLine();
				if(i == 0 || line.equals("")){
					++i;
					continue;
				}
				Scanner lineScanner = new Scanner(line);
				lineScanner.useDelimiter(",");
				
				String one = lineScanner.next();
				String two = lineScanner.next();
				String three = lineScanner.next();
				String four = lineScanner.next();
				String five = lineScanner.next();

				lineScanner.close();
				
				long date = -1;
				String source = two;
				String description = three;
				Account account = null;
				double cost = -1;
				
				try{
					if(!one.equals("")){
						SimpleDateFormat format = new SimpleDateFormat("MM/dd/yy");
						date = format.parse(one).getTime();
					}
					if(!four.equals("")){
						account = Account.fromString(four);
					}
					if(!five.equals("")){
						cost = Double.valueOf(five);
					}
					Income in = new Income(date, source, description, account, cost);
					if(in.getDescription().equals("Paycheck")){
						if(in.getDate() > paycheck){
							paycheck = in.getDate();
						}
					}
					if(!in.getStore().equals("Payday")){
						income.add(in);
					}
				}
				catch(ParseException e){
					errors.add("Income[" + (i + 1) + "] failed to parse date");
				}
				catch(AccountException e){
					errors.add("Income[" + (i + 1) + "] failed to read account");
				}
				catch(NumberFormatException e){
					errors.add("Income[" + (i + 1) + "] failed to parse cost");
				}
				++i;
			}
			scanner.close();
		}
		catch(IOException e){}
		return errors;
	}
	private List<String> readExpenses(){
		List<String> errors = new ArrayList<String>();
		try{
			File file = new File("/home/steven/Budget/expense.csv");
			Scanner scanner = new Scanner(file);
			int i = 0;
			while(scanner.hasNextLine()){
				String line = scanner.nextLine();
				if(i == 0 || line.equals("")){
					++i;
					continue;
				}
				Scanner lineScanner = new Scanner(line);
				lineScanner.useDelimiter(",");
				
				String one = lineScanner.next();
				String two = lineScanner.next();
				String three = lineScanner.next();
				String four = lineScanner.next();
				String five = lineScanner.next();
				String six = lineScanner.next();

				lineScanner.close();
				
				long date = 0;
				String source = two;
				Category category = null;
				String description = four;
				Account account = null;
				double cost = -1;
				
				try{
					if(!one.equals("")){
						SimpleDateFormat format = new SimpleDateFormat("MM/dd/yy");
						date = format.parse(one).getTime();
					}
					if(!three.equals("")){
						category = Category.fromString(three);
					}
//					if(!five.equals("")){
						account = Account.fromString(five);
//					}
					if(!six.equals("")){
						cost = Double.valueOf(six);
					}
					expenses.add(new Expense(date, source, category, description, account, cost));
				}
				catch(ParseException e){
					errors.add("Expense[" + (i + 1) + "] failed to parse date");
				}
				catch(CategoryException e){
					errors.add("Expense[" + (i + 1) + "] failed to read category");
				}
				catch(AccountException e){
					errors.add("Expense[" + (i + 1) + "] failed to read account");
				}
				catch(NumberFormatException e){
					errors.add("Expense[" + (i + 1) + "] failed to parse cost");
				}
				++i;
			}
			scanner.close();
		}
		catch(IOException e){}
		return errors;
	}
	private List<String> readBudget(){
		List<String> errors = new ArrayList<String>();
		try{
			File file = new File("/home/steven/Budget/budget.sp");
			Scanner scanner = new Scanner(file);
			int i = 0;
			while(scanner.hasNextLine()){
				String line = scanner.nextLine();
				if(line.equals("")){
					++i;
					continue;
				}
				Scanner lineScanner = new Scanner(line);
				lineScanner.useDelimiter("\t");
				
				String one = lineScanner.next();
				String two = line.substring(line.lastIndexOf('\t') + 1);

				lineScanner.close();

				Category category = null;
				double monthly = 0;
				
				try{
					category = Category.fromString(one);
					monthly = Double.valueOf(two);
					budget.put(category, monthly);
				}
				catch(CategoryException e){
					errors.add("Budget[" + (i + 1) + "] failed to read category");
				}
				catch(NumberFormatException e){
					errors.add("Budget[" + (i + 1) + "] failed to parse budget");
				}
				++i;
			}
			scanner.close();
		}
		catch(IOException e){}
		return errors;
	}
	private List<String> readBalance(){
		List<String> errors = new ArrayList<String>();
		try{
			File file = new File("/home/steven/Budget/account.sp");
			Scanner scanner = new Scanner(file);
			int i = 0;
			while(scanner.hasNextLine()){
				String line = scanner.nextLine();
				if(line.equals("") || line.startsWith("-")){
					++i;
					continue;
				}
				Scanner lineScanner = new Scanner(line);
				lineScanner.useDelimiter("\t");
				
				String one = lineScanner.next();
				String two = line.substring(line.lastIndexOf('\t') + 1);

				lineScanner.close();

				Account account = null;
				double bal = 0;
				
				try{
					account = Account.fromString(one);
					bal = Double.valueOf(two);
					balance.put(account, bal);
				}
				catch(AccountException e){
					errors.add("Balance[" + (i + 1) + "] failed to read account");
				}
				catch(NumberFormatException e){
					errors.add("Balance[" + (i + 1) + "] failed to parse balance");
				}
				++i;
			}
			scanner.close();
		}
		catch(IOException e){}
		return errors;
	}
	private List<String> readLastUpdated(){
		List<String> errors = new ArrayList<String>();
		try{
			File file = new File("/home/steven/Budget/lastUpdated.sp");
			Scanner scanner = new Scanner(file);
			while(scanner.hasNextLine()){
				String line = scanner.nextLine();
				SimpleDateFormat format = new SimpleDateFormat("MM/dd/yy");
				long last = 0;
				try{
					last = format.parse(line).getTime();
				}
				catch(ParseException e){
					errors.add("LastUpdated[1] failed to parse Last Updated date");
				}
				lastUpdated = last;
			}
			scanner.close();
		}
		catch(IOException e){}
		return errors;
	}
	private List<String> readSchedule(){
		List<String> errors = new ArrayList<String>();
		try{
			File file = new File("/home/steven/Budget/schedule.csv");
			Scanner scanner = new Scanner(file);
			int i = 0;
			while(scanner.hasNextLine()){
				String line = scanner.nextLine();
				if(i == 0 || line.equals("")){
					++i;
					continue;
				}
				Scanner lineScanner = new Scanner(line);
				lineScanner.useDelimiter(",");
				
				String one = lineScanner.next();
				String two = lineScanner.next();
				String three = lineScanner.next();
				String four = lineScanner.next();
				String five = lineScanner.next();
				String six = lineScanner.next();
				String seven = lineScanner.next();

				lineScanner.close();
				
				String company = one;
				double price = -1;
				int dueDate = -1;
				String payment = four;
				boolean automatic = false;
				int months = -1;
				int start = -1;
				
				try{
					price = Double.valueOf(two);
					dueDate = Integer.valueOf(three);
					if(five.equals("Yes")){
						automatic = true;
					}
					months = Integer.valueOf(six);
					start = Integer.valueOf(seven);
					schedule.add(new ScheduledExpense(company, price, dueDate, payment, automatic, months, start));
				}
				catch(NumberFormatException e){
					errors.add("Schedule[" + (i + 1) + "] failed to parse");
				}
				++i;
			}
			scanner.close();
		}
		catch(IOException e){}
		return errors;
	}
	
	//Frame
	public double getTithing(int year){
		double start = -1;
		double end = -1;
		try{
			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
			start = format.parse("01/01/" + year).getTime();
			end = format.parse("12/31/" + year).getTime();
		}
		catch(ParseException e){
			e.printStackTrace();
		}
		double tithe = 0;
		for(Income inc : income){
			if(inc.getDate() >= start && inc.getDate() <= end && inc.getDescription().equals("Paycheck")){
				tithe += (inc.getCost()/10);
			}
		}
		for(Expense exp : expenses){
			if(exp.getDate() >= start && exp.getDate() <= end && exp.getCategory().equals(Category.TITHING)){
				tithe -= exp.getCost();
			}
		}
		return tithe;
	}
	public String getLastUpdated(){
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
		return format.format(new Date(lastUpdated));
	}
	
	//Main
	public double getValue(Account account, int year){
		double start = -1;
		double end = -1;
		try{
			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
			start = format.parse("01/01/" + year).getTime();
			end = format.parse("12/31/" + year).getTime();
		}
		catch(ParseException e){
			e.printStackTrace();
		}
		double val = 0;
		for(Expense exp : expenses){
			if(exp.getAccount() == account && exp.getDate() >= start && exp.getDate() <= end){
				val -= exp.getCost();
			}
		}
		for(Income inc : income){
			if(inc.getAccount() == account && inc.getDate() >= start && inc.getDate() <= end){
				val += inc.getCost();
			}
		}
		return val;
	}
	public double getValue(Category category, int year, int currentMonth){
		++currentMonth;
		int tempYear = Calendar.getInstance().get(Calendar.YEAR);
		if(year != tempYear){
			currentMonth = 12;
		}
		double start = -1;
		double end = -1;
		try{
			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
			start = format.parse("01/01/" + year).getTime();
			end = format.parse("12/31/" + year).getTime();
		}
		catch(ParseException e){
			e.printStackTrace();
		}
		double val = 0;
		for(Expense exp : expenses){
			if(exp.getCategory() == category && exp.getDate() >= start && exp.getDate() <= end){
				val -= exp.getCost();
			}
		}
		if(budget.containsKey(category)){
			val += (budget.get(category) * currentMonth);
		}
		return val;
	}
	public double getBalance(Account account){
		if(balance.containsKey(account)){
			return balance.get(account);
		}
		return 0;
	}
	
	//Income
	public List<Income> getIncome(Account account, long start, long end){
		List<Income> result = new ArrayList<Income>();
		for(Income inc : income){
			if(inc.getDate() == -1 || (inc.getDate() >= start && inc.getDate() <= end)){
				if(account == null || inc.getAccount() == null || inc.getAccount() == account){
					result.add(inc);
				}
			}
		}
		Collections.sort(result);
		return result;
	}
	public String[] getPayee(int year){
		double start = -1;
		double end = -1;
		try{
			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
			start = format.parse("01/01/" + year).getTime();
			end = format.parse("12/31/" + year).getTime();
		}
		catch(ParseException e){
			e.printStackTrace();
		}
		Set<String> result = new TreeSet<String>();
		for(Income inc : income){
			if(inc.getDate() == -1 || (inc.getDate() >= start && inc.getDate() <= end)){
				if(!inc.getStore().equals("")){
					result.add(inc.getStore());
				}
			}
		}
		String[] returned = new String[result.size() + 1];
		returned[0] = "Source";
		int i = 1;
		for(String str : result){
			returned[i++] = str;
		}
		return returned;
	}
	
	//Expenses
	public List<Expense> getExpense(Account account, Category category, long start, long end){
		List<Expense> result = new ArrayList<Expense>();
		for(Expense exp : expenses){
			if(exp.getDate() == -1 || (exp.getDate() >= start && exp.getDate() <= end)){
				if(account == null || exp.getAccount() == null || exp.getAccount() == account){
					if(category == null || exp.getCategory() == null || exp.getCategory() == category){
						result.add(exp);
					}
				}
			}
		}
		Collections.sort(result);
		return result;
	}
	public String[] getStore(int year){
		double start = -1;
		double end = -1;
		try{
			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
			start = format.parse("01/01/" + year).getTime();
			end = format.parse("12/31/" + year).getTime();
		}
		catch(ParseException e){
			e.printStackTrace();
		}
		Set<String> result = new TreeSet<String>();
		for(Expense exp : expenses){
			if(exp.getDate() == -1 || (exp.getDate() >= start && exp.getDate() <= end)){
				if(!exp.getStore().equals("")){
					result.add(exp.getStore());
				}
			}
		}
		String[] returned = new String[result.size() + 1];
		returned[0] = "Store";
		int i = 1;
		for(String str : result){
			returned[i++] = str;
		}
		return returned;
	}
	
	//Schedule
	public List<Long> getPaydays(int year){
		double start = -1;
		double end = -1;
		try{
			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
			start = format.parse("01/01/" + year).getTime();
			end = format.parse("12/31/" + year).getTime();
		}
		catch(ParseException e){
			e.printStackTrace();
		}
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date(paycheck));
		while(cal.getTimeInMillis() >= start && start != -1){
			cal.add(Calendar.DAY_OF_MONTH, PAYCHECK_FREQUENCY * -7);
		}
		cal.add(Calendar.DAY_OF_MONTH, PAYCHECK_FREQUENCY * 7);

		List<Long> dates = new ArrayList<Long>();
		while(cal.getTimeInMillis() < end){
			dates.add(cal.getTimeInMillis());
			cal.add(Calendar.DAY_OF_MONTH, PAYCHECK_FREQUENCY * 7);
		}
		return dates;
	}
	public List<ScheduledExpense> getScheduledExpenses(long date){
		Date payDate = new Date(date);
		Calendar cal = Calendar.getInstance();
		cal.setTime(payDate);
		PriorityQueue q = new PriorityQueue();
		for(ScheduledExpense ex : schedule){
			Calendar cPay = (Calendar) cal.clone();
			int pay = ex.getDueDate();
			cPay.set(Calendar.DAY_OF_MONTH, pay);
			if(!ex.isAuto()){
				cPay.add(Calendar.DAY_OF_MONTH, -DAYS_BEFORE);
			}
			if(cPay.before(cal)){
				cPay.add(Calendar.MONTH, 1);
			}
			
			int diff = (int) TimeUnit.DAYS.convert(cPay.getTimeInMillis() - cal.getTimeInMillis(), TimeUnit.MILLISECONDS);
			if(diff > 0 && diff <= PAYCHECK_FREQUENCY * 7){
				int m = cPay.get(Calendar.MONTH) + 1;
				int frequency = ex.getMonths();
				int start = ex.getStart();
				for(int i = 0; i < 12; ++i){
					if(start == m){
						q.add(ex, diff);
						break;
					}
					start += frequency;
				}
			}
		}
		return q.getList();
	}
}

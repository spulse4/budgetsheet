package tabs;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import definitions.Account;
import definitions.Income;
import exceptions.AccountException;

import main.Model;

public class IncomeTab extends JPanel{

	private static final long serialVersionUID = -7071166635853787315L;

	private int year;
	private long start;
	private long end;
	private String pay;
	private Account acc;
	private List<JCheckBox> boxes;
	private List<Income> income;
	
	public IncomeTab(){
		year = Calendar.getInstance().get(Calendar.YEAR);
		start = -1;
		end = -1;
		pay = "";
		acc = null;
		income = Model.getInstance().getIncome(acc, start, end);
		boxes = new ArrayList<JCheckBox>();
		for(int i = 0; i < income.size(); ++i){
			boxes.add(new JCheckBox());
		}
		init();
	}
	
	public void setYear(int year){
		this.year = year;
		this.start = -1;
		this.end = -1;
		this.pay = "";
		this.acc = null;
		income = Model.getInstance().getIncome(acc, start, end);
		boxes = new ArrayList<JCheckBox>();
		for(int i = 0; i < income.size(); ++i){
			boxes.add(new JCheckBox());
		}
		init();
	}
	
	private void init(){
		initTop();
		initMid();
	}
	private void initTop(){
		this.removeAll();
		this.setLayout(new BorderLayout());
		int monthLength = 0;
		int payeeLength = 0;
		int accountLength = 0;
		String[] monthArray = new String[13];
		String[] payeeArray = Model.getInstance().getPayee(year);
		String[] accountArray = new String[Account.values().length + 1];

		monthArray[monthLength++] = "Month";
		monthArray[monthLength++] = "January";
		monthArray[monthLength++] = "February";
		monthArray[monthLength++] = "March";
		monthArray[monthLength++] = "April";
		monthArray[monthLength++] = "May";
		monthArray[monthLength++] = "June";
		monthArray[monthLength++] = "July";
		monthArray[monthLength++] = "August";
		monthArray[monthLength++] = "September";
		monthArray[monthLength++] = "October";
		monthArray[monthLength++] = "November";
		monthArray[monthLength++] = "December";
		
		accountArray[0] = "Account";
		for(int i = 0; i < Account.values().length; ++i){
			accountArray[i + 1] = Account.values()[i].getString();
		}
		
		final JComboBox<String> month = new JComboBox<String>(monthArray);
		final JComboBox<String> payee = new JComboBox<String>(payeeArray);
		final JComboBox<String> account = new JComboBox<String>(accountArray);
		
		ActionListener action = new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				if(e.getSource() == month){
					String m = (String) month.getSelectedItem();
					if(m.equals("Month")){
						start = -1;
						end = -1;
						initMid();
						return;
					}
					String monthString = "";
					String endDate = "";
					switch(m){
					case "January":
						monthString = "01";
						endDate = "31";
						break;
					case "February":
						monthString = "02";
						if(year % 4 == 0){
							endDate = "29";
						}
						else{
							endDate = "28";
						}
						break;
					case "March":
						monthString = "03";
						endDate = "31";
						break;
					case "April":
						monthString = "04";
						endDate = "30";
						break;
					case "May":
						monthString = "05";
						endDate = "31";
						break;
					case "June":
						monthString = "06";
						endDate = "30";
						break;
					case "July":
						monthString = "07";
						endDate = "31";
						break;
					case "August":
						monthString = "08";
						endDate = "31";
						break;
					case "September":
						monthString = "09";
						endDate = "30";
						break;
					case "October":
						monthString = "10";
						endDate = "31";
						break;
					case "November":
						monthString = "11";
						endDate = "30";
						break;
					case "December":
						monthString = "12";
						endDate = "31";
						break;
					}
					try{
						SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
						start = format.parse(monthString + "/01/" + year).getTime();
						end = format.parse(monthString + "/" + endDate + "/" + year).getTime();
					}
					catch(ParseException ex){
						ex.printStackTrace();
					}
					income = Model.getInstance().getIncome(acc, start, end);
					boxes = new ArrayList<JCheckBox>();
					for(int i = 0; i < income.size(); ++i){
						boxes.add(new JCheckBox());
					}
				}
				else if(e.getSource() == payee){
					String p = (String) payee.getSelectedItem();
					if(p.equals("Source")){
						pay = "";
					}
					else{
						pay = p;
					}
				}
				else if(e.getSource() == account){
					String a = (String) account.getSelectedItem();
					if(a.equals("Account")){
						acc = null;
						initMid();
						return;
					}
					try{
						Account ac = Account.fromString(a);
						acc = ac;
					}
					catch(AccountException ex){
						ex.printStackTrace();
					}
				}
				initMid();
			}
			
		};
		
		month.addActionListener(action);
		payee.addActionListener(action);
		account.addActionListener(action);
		
		Font f = month.getFont();
		FontMetrics fm = this.getFontMetrics(f);
		
		monthLength = 0;
		for(String str : monthArray){
			int width = fm.stringWidth(str);
			if(width > monthLength){
				monthLength = width;
			}
		}
		for(String str : payeeArray){
			int width = fm.stringWidth(str);
			if(width > payeeLength){
				payeeLength = width;
			}
		}
		for(String str : accountArray){
			int width = fm.stringWidth(str);
			if(width > accountLength){
				accountLength = width;
			}
		}
		
		month.setPreferredSize(new Dimension(monthLength + 30, 24));
		payee.setPreferredSize(new Dimension(payeeLength + 30, 24));
		account.setPreferredSize(new Dimension(accountLength + 30, 24));
		
		JPanel top = new JPanel(new FlowLayout(FlowLayout.CENTER));
		top.add(month);
		top.add(payee);
		top.add(account);
		
		this.add(top, BorderLayout.NORTH);
		this.revalidate();
		this.repaint();
	}
	
	private void initMid(){
		if(this.getComponentCount() > 1){
			this.remove(1);
		}
		JPanel mid = new JPanel();
		if(start == -1){
			JPanel temp = new JPanel();
			temp.add(new JLabel("No Results"));
			this.add(temp, BorderLayout.CENTER);
			this.revalidate();
			this.repaint();
			return;
		}
		mid.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
				
		List<Income> filtered = new ArrayList<Income>();
		List<JCheckBox> filteredCheck = new ArrayList<JCheckBox>();
		for(int i = 0; i < income.size(); ++i){
			Income in = income.get(i);
			if(pay.equals("") || pay.equals(in.getStore())){
				if(acc == null || acc == in.getAccount()){
					filtered.add(in);
					filteredCheck.add(boxes.get(i));
				}
			}
		}
		if(filtered.size() == 0){
			JPanel temp = new JPanel();
			temp.add(new JLabel("No Results"));
			this.add(temp, BorderLayout.CENTER);
			this.revalidate();
			this.repaint();
			return;
		}
		
		Insets inset = new Insets(0, 10, 5, 10);
		JLabel date1 = new JLabel("Date");
		JLabel source1 = new JLabel("Source");
		JLabel desc1 = new JLabel("Description");
		JLabel accou1 = new JLabel("Account");
		JLabel price1 = new JLabel("Amount");
		JLabel check = new JLabel("Verified");
		c.anchor = GridBagConstraints.NORTHWEST;
		c.gridx = 0;
		c.gridy = 0;
		c.insets = inset;
		c.weightx = 0.0;
		c.weighty = 0.0;

		mid.add(date1, c);
		c.gridx = 1;
		mid.add(source1, c);
		c.gridx = 2;
		mid.add(accou1, c);
		c.gridx = 3;
		mid.add(price1, c);
		c.anchor = GridBagConstraints.NORTH;
		c.gridx = 4;
		mid.add(check, c);
		c.anchor = GridBagConstraints.NORTHWEST;
		c.gridx = 5;
		c.weightx = 1.0;
		mid.add(desc1, c);

		for(int i = 0; i < filtered.size(); ++i){
			Income in = filtered.get(i);
			JLabel date = new JLabel(in.getDateFormatted());
			JLabel source = new JLabel(in.getStore());
			JLabel desc = new JLabel(in.getDescription());
			JLabel accou = new JLabel(in.getAccount().getAbbreviated());
			JLabel price = new JLabel(in.getCostFormatted());
			JCheckBox box = filteredCheck.get(i);
			
			c.anchor = GridBagConstraints.NORTHWEST;
			c.gridx = 0;
			c.gridy = i + 1;
			c.insets = inset;
			c.weightx = 0.0;
			c.weighty = 0.0;
			if(i == filtered.size() - 1){
				c.weighty = 1.0;
			}
			
			mid.add(date, c);
			c.gridx = 1;
			mid.add(source, c);
			c.gridx = 2;
			mid.add(accou, c);
			c.anchor = GridBagConstraints.NORTHEAST;
			c.gridx = 3;
			mid.add(price, c);
			c.anchor = GridBagConstraints.NORTH;
			c.gridx = 4;
			mid.add(box, c);
			c.anchor = GridBagConstraints.NORTHWEST;
			c.gridx = 5;
			c.weightx = 1.0;
			mid.add(desc, c);
		}
		
		this.add(mid, BorderLayout.CENTER);
		this.revalidate();
		this.repaint();
	}
}

package tabs;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import definitions.ScheduledExpense;

import main.Model;

public class ScheduleTab extends JPanel{

	private static final long serialVersionUID = -8699446692167630360L;

	private int year;

	public ScheduleTab(){
		year = Calendar.getInstance().get(Calendar.YEAR);
		init();
	}

	public void setYear(int year){
		this.year = year;
		init();
	}

	private void init(){
		this.removeAll();
		this.setLayout(new GridBagLayout());
		List<Long> paydays = Model.getInstance().getPaydays(year);
		if(year == Calendar.getInstance().get(Calendar.YEAR)){
			long d = -1;
			while(paydays.size() > 0 && paydays.get(0) < Calendar.getInstance().getTimeInMillis()){
				d = paydays.remove(0);
			}
			if(d != -1){
				paydays.add(0, d);
			}
		}
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		DecimalFormat df = new DecimalFormat("#,##0.00");
		
		Insets topIn = new Insets(0, 10, 0, 10);
		Insets bottomIn = new Insets(0, 10, 10, 10);
		
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.NORTHWEST;
		c.gridx = 0;
		c.gridy = 0;
		c.insets = bottomIn;
		c.weightx = 0.0;
		c.weighty = 0.0;
		this.add(new JLabel("Date"), c);
		c.gridx = 1;
		c.insets = bottomIn;
		this.add(new JLabel("Company"), c);
		c.anchor = GridBagConstraints.NORTH;
		c.gridx = 2;
		this.add(new JLabel("Price/Month"), c);
		c.gridx = 3;
		this.add(new JLabel("Due"), c);
		c.gridx = 4;
		this.add(new JLabel("Account"), c);
		c.gridx = 5;
		this.add(new JLabel("Automatic"), c);
		c.gridx = 6;
		c.weightx = 1.0;
		JCheckBox b = new JCheckBox();
		b.setEnabled(false);
		this.add(b, c);
		
		int index = 1;
		
		for(int i = 0; i < paydays.size(); ++i){
			long l = paydays.get(i);
			List<ScheduledExpense> expense = Model.getInstance().getScheduledExpenses(l);
			Date d = new Date(l);

			c.anchor = GridBagConstraints.NORTHWEST;
			c.gridx = 0;
			c.gridy = index;
			c.insets = topIn;
			c.weightx = 0.0;
			c.weighty = 0.0;
			if(i == paydays.size() - 1){
				c.weighty = 1.0;
			}
			this.add(new JLabel(sdf.format(d)), c);
			
			if(expense.size() == 0){
				++index;
			}
			for(int j = 0; j < expense.size(); ++j){
				ScheduledExpense ex = expense.get(j);
				JLabel one = new JLabel(ex.getExpense());
				JLabel two = new JLabel(df.format(ex.getPrice()));
				JLabel three = new JLabel(Integer.toString(ex.getDueDate()));
				JLabel four = new JLabel(ex.getPayment());
				JLabel five;
				if(ex.isAuto()){
					five = new JLabel("Automatic");
				}
				else{
					five = new JLabel("Manual");
				}
				JCheckBox box = new JCheckBox();
				
				c.anchor = GridBagConstraints.NORTHWEST;
				c.gridx = 1;
				c.gridy = index++;
				c.insets = topIn;
				c.weightx = 0.0;
				c.weighty = 0.0;
				if(j == expense.size() - 1){
					c.weighty = 1.0;
					c.insets = bottomIn;
				}
				this.add(one, c);
				c.anchor = GridBagConstraints.NORTHEAST;
				c.gridx = 2;
				this.add(two, c);
				c.anchor = GridBagConstraints.NORTH;
				c.gridx = 3;
				this.add(three, c);
				c.gridx = 4;
				this.add(four, c);
				c.gridx = 5;
				this.add(five, c);
				c.gridx = 6;
				c.weightx = 1.0;
				this.add(box, c);
			}
		}
	}	
}

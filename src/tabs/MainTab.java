package tabs;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.text.DecimalFormat;
import java.util.Calendar;

import javax.swing.JLabel;
import javax.swing.JPanel;

import main.Model;
import definitions.Account;
import definitions.Category;

public class MainTab extends JPanel{

	private static final long serialVersionUID = 7421144646355962048L;

	private int year;
	
	public MainTab(){
		year = Calendar.getInstance().get(Calendar.YEAR);
		init();
	}
	
	public void setYear(int year){
		this.year = year;
		init();
	}
	
	private void init(){
		this.removeAll();
		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		JLabel categoryHeader = new JLabel("Category");
		JLabel categoryRemaining1 = new JLabel("Remaining");
		JLabel categoryRemaining2 = new JLabel("Current");
		JLabel categoryRemaining3 = new JLabel("Balance");
		JLabel accountHeader = new JLabel("Account");
		JLabel accountRemainingA1 = new JLabel("Remaining");
		JLabel accountRemainingA2 = new JLabel("Calculated");
		JLabel accountRemainingA3 = new JLabel("Balance");
		JLabel accountRemainingB1 = new JLabel("Remaining");
		JLabel accountRemainingB2 = new JLabel("Actual");
		JLabel accountRemainingB3 = new JLabel("Balance");
		
		Insets bottom = new Insets(0, 10, 5, 10);
		Insets noBottom = new Insets(0, 10, 0, 10);
		Insets noLeft = new Insets(0, 0, 0, 10);
		
		c.anchor = GridBagConstraints.NORTHWEST;
		c.gridx = 0;
		c.gridy = 1;
		c.insets = noBottom;
		c.weightx = 0.0;
		c.weighty = 0.0;		
		this.add(categoryHeader, c);
		c.gridx = 2;
		this.add(accountHeader, c);
		c.anchor = GridBagConstraints.NORTHEAST;
		c.gridx = 1;
		c.gridy = 0;
		c.weightx = 1.0;
		this.add(categoryRemaining1, c);
		c.gridy = 1;
		this.add(categoryRemaining2, c);
		c.gridy = 2;
		c.insets = bottom;
		this.add(categoryRemaining3, c);
		c.gridx = 3;
		c.gridy = 0;
		c.insets = noBottom;
		this.add(accountRemainingA1, c);
		c.gridy = 1;
		this.add(accountRemainingA2, c);
		c.gridy = 2;
		c.insets = bottom;
		this.add(accountRemainingA3, c);
		c.gridx = 4;
		c.gridy = 0;
		c.insets = noBottom;
		c.weightx = 0.0;
		this.add(accountRemainingB1, c);
		c.gridy = 1;
		this.add(accountRemainingB2, c);
		c.gridy = 2;
		c.insets = bottom;
		this.add(accountRemainingB3, c);
				
		int currentMonth = Calendar.getInstance().get(Calendar.MONTH);
		
		DecimalFormat df = new DecimalFormat("$#,##0.00");

		for(int i = 0; i < Category.values().length; ++i){
			Category cat = Category.values()[i];
			c.anchor = GridBagConstraints.NORTHWEST;
			c.gridx = 0;
			c.gridy = i + 3;
			c.gridheight = 1;
			c.gridwidth = 1;
			c.insets = noBottom;
			c.weightx = 0.0;
			c.weighty = 0.0;
			if(i == Category.values().length - 1){
				if(Category.values().length >= Account.values().length + 6){
					c.insets = bottom;
				}
				c.gridheight = GridBagConstraints.REMAINDER;
				c.weighty = 1.0;
			}
			JLabel categoryLabel = new JLabel(cat.getString());
			this.add(categoryLabel, c);
			double amount = Model.getInstance().getValue(cat, year, currentMonth);
			JLabel categoryLeft = new JLabel(df.format(amount));
			if(amount < 0){
				categoryLeft.setForeground(Color.RED);
			}
			c.anchor = GridBagConstraints.NORTHEAST;
			c.gridx = 1;
			c.insets = noLeft;
			c.weightx = 1.0;
			this.add(categoryLeft, c);
		}
		
		double debit = 0;
		double debt = 0;
		double credit = 0;
		
		int diff = 0;
		for(int i = 0; i < Account.values().length; ++i){
			Account acc = Account.values()[i];
			if(acc == Account.N_A){
				diff = 1;
				continue;
			}
			c.anchor = GridBagConstraints.NORTHWEST;
			c.gridx = 2;
			c.gridy = i + 3 + diff;
			c.gridheight = 1;
			c.gridwidth = 1;
			c.insets = noBottom;
			c.weightx = 0.0;
			c.weighty = 0.0;
			JLabel accountLabel = new JLabel(acc.getString());
			this.add(accountLabel, c);
			double amount = Model.getInstance().getValue(acc, year);
			JLabel accountLeft = new JLabel(df.format(amount));
			double amount2 = Model.getInstance().getBalance(acc);
			JLabel accountActual = new JLabel(df.format(amount2));
			if(Math.abs(amount - amount2) >= .01){
				accountLeft.setForeground(Color.RED);
				accountActual.setForeground(Color.RED);
			}
			String type = acc.getType();
			if(type.equals("Credit")){
				accountLabel.setForeground(Color.BLUE);
				credit += amount2;
			}
			else if(type.equals("Debit")){
				debit += amount2;
				accountLabel.setForeground(Color.BLACK);
			}
			else if(type.equals("Debt")){
				debt += amount2;
				accountLabel.setForeground(Color.MAGENTA);
			}
			else{
				accountLabel.setForeground(Color.RED);
			}
			c.anchor = GridBagConstraints.NORTHEAST;
			c.gridx = 3;
			c.insets = noLeft;
			c.weightx = 1.0;
			this.add(accountLeft, c);
			c.gridx = 4;
			this.add(accountActual, c);
		}
		int i = Account.values().length;
		boolean go = true;
		if(Account.values().length < Category.values().length){
			++i;
			go = false;
		}

		Insets inB = new Insets(10, 10, 0, 10);
		Insets inL = new Insets(10, 0, 0, 10);
		c.anchor = GridBagConstraints.NORTHWEST;
		c.gridx = 2;
		c.gridy = i + 3;
		c.gridheight = 1;
		c.gridwidth = 2;
		if(go){
			c.insets = inB;
		}
		else{
			c.insets = noBottom;
		}
		c.weightx = 0.0;
		c.weighty = 0.0;
		
		JLabel debt1 = new JLabel("Debt");
		JLabel debit1 = new JLabel("Debit");
		JLabel credit1 = new JLabel("Credit");
		JLabel available1 = new JLabel("Available");
		JLabel all1 = new JLabel("All");
		JLabel debt2 = new JLabel(df.format(debt));
		JLabel debit2 = new JLabel(df.format(debit));
		JLabel credit2 = new JLabel(df.format(credit));
		JLabel available2 = new JLabel(df.format(debit + credit));
		JLabel all2 = new JLabel(df.format(debt + debit + credit));
		this.add(debt1, c);
		c.anchor = GridBagConstraints.NORTHEAST;
		c.gridx = 3;
		if(go){
			c.insets = inL;
		}
		else{
			c.insets = noLeft;
		}
		c.weightx = 1.0;
		this.add(debt2, c);
		
		c.anchor = GridBagConstraints.NORTHWEST;
		c.gridx = 2;
		c.gridy = i + 4;
		c.insets = noBottom;
		c.weightx = 0.0;
		this.add(debit1, c);
		c.anchor = GridBagConstraints.NORTHEAST;
		c.gridx = 3;
		c.insets = noLeft;
		c.weightx = 1.0;
		this.add(debit2, c);

		c.anchor = GridBagConstraints.NORTHWEST;
		c.gridx = 2;
		c.gridy = i + 5;
		c.insets = noBottom;
		c.weightx = 0.0;
		this.add(credit1, c);
		c.anchor = GridBagConstraints.NORTHEAST;
		c.gridx = 3;
		c.insets = noLeft;
		c.weightx = 1.0;
		this.add(credit2, c);

		c.anchor = GridBagConstraints.NORTHWEST;
		c.gridx = 2;
		c.gridy = i + 6;
		c.insets = noBottom;
		c.weightx = 0.0;
		this.add(available1, c);
		c.anchor = GridBagConstraints.NORTHEAST;
		c.gridx = 3;
		c.insets = noLeft;
		c.weightx = 1.0;
		this.add(available2, c);

		c.anchor = GridBagConstraints.NORTHWEST;
		c.gridx = 2;
		c.gridy = i + 7;
		c.gridheight = GridBagConstraints.REMAINDER;
		c.insets = noBottom;
		if(Account.values().length + 6 >= Category.values().length){
			c.insets = bottom;
		}
		c.weightx = 0.0;
		c.weighty = 1.0;
		this.add(all1, c);
		c.anchor = GridBagConstraints.NORTHEAST;
		c.gridx = 3;
		c.insets = noLeft;
		c.weightx = 1.0;
		this.add(all2, c);
	}
}

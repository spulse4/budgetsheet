package tabs;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import main.Model;

import definitions.Account;
import definitions.Category;
import definitions.Expense;
import exceptions.AccountException;
import exceptions.CategoryException;

public class ExpensesTab extends JPanel{

	private static final long serialVersionUID = 1389422827163292276L;

	private int year;
	private long start;
	private long end;
	private String sto;
	private Category cat;
	private Account acc;
	private List<JCheckBox> boxes;
	private List<Expense> expense;
	
	public ExpensesTab(){
		year = Calendar.getInstance().get(Calendar.YEAR);
		start = -1;
		end = -1;
		sto = "";
		cat = null;
		acc = null;
		expense = Model.getInstance().getExpense(acc, cat, start, end);
		boxes = new ArrayList<JCheckBox>();
		for(int i = 0; i < expense.size(); ++i){
			boxes.add(new JCheckBox());
		}
		init();
	}
	
	public void setYear(int year){
		this.year = year;
		this.start = -1;
		this.end = -1;
		this.sto = "";
		this.cat = null;
		this.acc = null;
		expense = Model.getInstance().getExpense(acc, cat, start, end);
		boxes = new ArrayList<JCheckBox>();
		for(int i = 0; i < expense.size(); ++i){
			boxes.add(new JCheckBox());
		}
		init();
	}
	
	private void init(){
		initTop();
		initMid();
	}
	
	private void initTop(){
		this.removeAll();
		this.setLayout(new BorderLayout());
		int monthLength = 0;
		int payeeLength = 0;
		int categoryLength = 0;
		int accountLength = 0;
		String[] monthArray = new String[13];
		String[] storeArray = Model.getInstance().getStore(year);
		String[] categoryArray = new String[Category.values().length + 1];
		String[] accountArray = new String[Account.values().length + 1];

		monthArray[monthLength++] = "Month";
		monthArray[monthLength++] = "January";
		monthArray[monthLength++] = "February";
		monthArray[monthLength++] = "March";
		monthArray[monthLength++] = "April";
		monthArray[monthLength++] = "May";
		monthArray[monthLength++] = "June";
		monthArray[monthLength++] = "July";
		monthArray[monthLength++] = "August";
		monthArray[monthLength++] = "September";
		monthArray[monthLength++] = "October";
		monthArray[monthLength++] = "November";
		monthArray[monthLength++] = "December";
		
		categoryArray[0] = "Category";
		for(int i = 0; i < Category.values().length; ++i){
			categoryArray[i + 1] = Category.values()[i].getString();
		}
		
		accountArray[0] = "Account";
		for(int i = 0; i < Account.values().length; ++i){
			accountArray[i + 1] = Account.values()[i].getString();
		}
		
		final JComboBox<String> month = new JComboBox<String>(monthArray);
		final JComboBox<String> store = new JComboBox<String>(storeArray);
		final JComboBox<String> category = new JComboBox<String>(categoryArray);
		final JComboBox<String> account = new JComboBox<String>(accountArray);
		
		ActionListener action = new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				if(e.getSource() == month){
					String m = (String) month.getSelectedItem();
					if(m.equals("Month")){
						start = -1;
						end = -1;
						initMid();
						return;
					}
					String monthString = "";
					String endDate = "";
					switch(m){
					case "January":
						monthString = "01";
						endDate = "31";
						break;
					case "February":
						monthString = "02";
						if(year % 4 == 0){
							endDate = "29";
						}
						else{
							endDate = "28";
						}
						break;
					case "March":
						monthString = "03";
						endDate = "31";
						break;
					case "April":
						monthString = "04";
						endDate = "30";
						break;
					case "May":
						monthString = "05";
						endDate = "31";
						break;
					case "June":
						monthString = "06";
						endDate = "30";
						break;
					case "July":
						monthString = "07";
						endDate = "31";
						break;
					case "August":
						monthString = "08";
						endDate = "31";
						break;
					case "September":
						monthString = "09";
						endDate = "30";
						break;
					case "October":
						monthString = "10";
						endDate = "31";
						break;
					case "November":
						monthString = "11";
						endDate = "30";
						break;
					case "December":
						monthString = "12";
						endDate = "31";
						break;
					}
					try{
						SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
						start = format.parse(monthString + "/01/" + year).getTime();
						end = format.parse(monthString + "/" + endDate + "/" + year).getTime();
					}
					catch(ParseException ex){
						ex.printStackTrace();
					}
					expense = Model.getInstance().getExpense(acc, cat, start, end);
					boxes = new ArrayList<JCheckBox>();
					for(int i = 0; i < expense.size(); ++i){
						boxes.add(new JCheckBox());
					}
				}
				else if(e.getSource() == store){
					String s = (String) store.getSelectedItem();
					if(s.equals("Store")){
						sto = "";
					}
					else{
						sto = s;
					}
				}
				else if(e.getSource() == category){
					String a = (String) category.getSelectedItem();
					if(a.equals("Category")){
						cat = null;
						initMid();
						return;
					}
					try{
						Category ca = Category.fromString(a);
						cat = ca;
					}
					catch(CategoryException ex){
						ex.printStackTrace();
					}
				}
				else if(e.getSource() == account){
					String a = (String) account.getSelectedItem();
					if(a.equals("Account")){
						acc = null;
						initMid();
						return;
					}
					try{
						Account ac = Account.fromString(a);
						acc = ac;
					}
					catch(AccountException ex){
						ex.printStackTrace();
					}
				}
				initMid();
			}
			
		};
		
		month.addActionListener(action);
		store.addActionListener(action);
		category.addActionListener(action);
		account.addActionListener(action);
		
		Font f = month.getFont();
		FontMetrics fm = this.getFontMetrics(f);
		
		monthLength = 0;
		for(String str : monthArray){
			int width = fm.stringWidth(str);
			if(width > monthLength){
				monthLength = width;
			}
		}
		for(String str : storeArray){
			int width = fm.stringWidth(str);
			if(width > payeeLength){
				payeeLength = width;
			}
		}
		for(String str : categoryArray){
			int width = fm.stringWidth(str);
			if(width > categoryLength){
				categoryLength = width;
			}
		}
		for(String str : accountArray){
			int width = fm.stringWidth(str);
			if(width > accountLength){
				accountLength = width;
			}
		}
		
		month.setPreferredSize(new Dimension(monthLength + 30, 24));
		store.setPreferredSize(new Dimension(payeeLength + 30, 24));
		category.setPreferredSize(new Dimension(categoryLength + 30, 24));
		account.setPreferredSize(new Dimension(accountLength + 30, 24));
		
		JPanel top = new JPanel(new FlowLayout(FlowLayout.CENTER));
		top.add(month);
		top.add(store);
		top.add(category);
		top.add(account);
		
		this.add(top, BorderLayout.NORTH);
		this.revalidate();
		this.repaint();
	}
	
	private void initMid(){
		if(this.getComponentCount() > 1){
			this.remove(1);
		}
		JPanel mid = new JPanel();
		if(start == -1){
			JPanel temp = new JPanel();
			temp.add(new JLabel("No Results"));
			this.add(temp, BorderLayout.CENTER);
			this.revalidate();
			this.repaint();
			return;
		}
		mid.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
				
		List<Expense> filtered = new ArrayList<Expense>();
		List<JCheckBox> filteredCheck = new ArrayList<JCheckBox>();
		for(int i = 0; i < expense.size(); ++i){
			Expense ex = expense.get(i);
			if(sto.equals("") || sto.equals(ex.getStore())){
				if(cat == null || cat == ex.getCategory()){
					if(acc == null || acc == ex.getAccount()){
						filtered.add(ex);
						filteredCheck.add(boxes.get(i));
					}
				}
			}
		}
		if(filtered.size() == 0){
			JPanel temp = new JPanel();
			temp.add(new JLabel("No Results"));
			this.add(temp, BorderLayout.CENTER);
			this.revalidate();
			this.repaint();
			return;
		}
		
		Insets inset = new Insets(0, 10, 5, 10);
		JLabel date1 = new JLabel("Date");
		JLabel source1 = new JLabel("Source");
		JLabel cate1 = new JLabel("Category");
		JLabel desc1 = new JLabel("Description");
		JLabel accou1 = new JLabel("Account");
		JLabel price1 = new JLabel("Amount");
		JLabel check = new JLabel("Verified");
		c.anchor = GridBagConstraints.NORTHWEST;
		c.gridx = 0;
		c.gridy = 0;
		c.insets = inset;
		c.weightx = 0.0;
		c.weighty = 0.0;

		mid.add(date1, c);
		c.gridx = 1;
		mid.add(source1, c);
		c.gridx = 2;
		mid.add(cate1, c);
		c.gridx = 3;
		mid.add(accou1, c);
		c.gridx = 4;
		mid.add(price1, c);
		c.anchor = GridBagConstraints.NORTH;
		c.gridx = 5;
		mid.add(check, c);
		c.anchor = GridBagConstraints.NORTHWEST;
		c.gridx = 6;
		c.weightx = 1.0;
		mid.add(desc1, c);

		mid.setOpaque(false);
		
		for(int i = 0; i < filtered.size(); ++i){
			Expense ex = filtered.get(i);
			JLabel date = new JLabel(ex.getDateFormatted());
			JLabel source = new JLabel(ex.getStore());
			JLabel cate = new JLabel(ex.getCategory().getAbbreviated());
			JLabel desc = new JLabel(ex.getDescription());
			JLabel accou = new JLabel(ex.getAccount().getAbbreviated());
			JLabel price = new JLabel(ex.getCostFormatted());
			JCheckBox box = filteredCheck.get(i);
			
			c.anchor = GridBagConstraints.NORTHWEST;
			c.gridx = 0;
			c.gridy = i + 1;
			c.insets = inset;
			c.weightx = 0.0;
			c.weighty = 0.0;
			if(i == filtered.size() - 1){
				c.weighty = 1.0;
			}
			
			mid.add(date, c);
			c.gridx = 1;
			mid.add(source, c);
			c.gridx = 2;
			mid.add(cate, c);
			c.gridx = 3;
			mid.add(accou, c);
			c.anchor = GridBagConstraints.NORTHEAST;
			c.gridx = 4;
			mid.add(price, c);
			c.anchor = GridBagConstraints.NORTH;
			c.gridx = 5;
			mid.add(box, c);
			c.anchor = GridBagConstraints.NORTHWEST;
			c.gridx = 6;
			c.weightx = 1.0;
			mid.add(desc, c);
		}
		
		this.add(mid, BorderLayout.CENTER);
		this.revalidate();
		this.repaint();
	}
}

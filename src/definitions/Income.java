package definitions;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Income implements Comparable<Income>{

	private long date;
	private String store;
	private String description;
	private Account account;
	private double cost;
	
	public Income(long date, String store,
				   String description, Account account, double cost){
		this.date = date;
		this.store = store;
		this.description = description;
		this.account = account;
		this.cost = cost;
		if(this.store.length() > 14){
			this.store = this.store.substring(0, 10) + "...";
		}
		if(this.description.length() > 40){
			this.description = this.description.substring(0, 35) + "...";
		}

	}
	
	public long getDate(){
		return date;
	}
	public String getDateFormatted(){
		Date d = new Date(date);
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yy");
		return format.format(d);
	}
	public String getStore(){
		return store;
	}
	public String getDescription(){
		return description;
	}
	public Account getAccount(){
		return account;
	}
	public double getCost(){
		return cost;
	}
	public String getCostFormatted(){
		DecimalFormat df = new DecimalFormat("$#,##0.00");
		return df.format(cost);
	}

	@Override
	public int compareTo(Income o) {
		return Long.compare(this.date, o.date);
	}
}

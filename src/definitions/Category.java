package definitions;

import exceptions.CategoryException;

public enum Category {

	BABY,
	BIRTHDAY,
	BOOK,
	CABLE,
	CAR_MAINTENANCE,
	CAR_REGISTRATION,
	CHRISTMAS,
	CLEANING_SUPPLIES,
	CLOTHES,
	COOKING,
	DINNERWARE,
	ELECTRONICS,
	FAST_OFFERING,
	FOOD,
	FUN,
	FURNITURE,
	GARDEN,
	GAS,
	GIFT,
	HEALTH_AND_BEAUTY,
	HOME_APPLIANCES,
	HOME_DECOR,
	HOME_GOODS,
	INSURANCE,
	INTEREST,
	INTERNET,
	KITCHEN_APPLIANCES,
	MISC,
	MOVIES,
	MUSIC,
	OFFICE_SUPPLIES,
	PET,
	PHONE,
	POSTAGE,
	RENT,
	SCHOOL,
	SPORTS,
	TAX,
	TITHING,
	TOOLS,
	TRANSPORTATION,
	TUITION,
	UTILITIES;
		
	public static Category fromString(String str) throws CategoryException{
		str = str.toLowerCase();
		str = str.replaceAll("_", " ");
		for(Category cat : Category.values()){
			String a = cat.toString().toLowerCase();
			a = a.replaceAll("_", " ");
			if(a.equals(str)){
				return cat;
			}
		}
		throw new CategoryException();
	}
	
	public String getString(){
		String temp = this.toString().toLowerCase();
		StringBuilder str = new StringBuilder();
		boolean space = true;
		for(int i = 0; i < temp.length(); ++i){
			if(space){
				str.append(Character.toUpperCase(temp.charAt(i)));
				space = false;
			}
			else{
				if(temp.charAt(i) == '_'){
					str.append(" ");
					space = true;
				}
				else{
					str.append(temp.charAt(i));
				}
			}
		}
		return str.toString();
	}
	
	public String getAbbreviated(){
		String temp = getString();
		temp = temp.replaceAll(" And ", " & ");
		return temp;
	}
}

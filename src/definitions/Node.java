package definitions;

public class Node {

	private int rank;
	private ScheduledExpense s;
	
	public Node(ScheduledExpense s, int rank){
		this.s = s;
		this.rank = rank;
	}
	
	public int getRank(){
		return rank;
	}
	public boolean isAuto(){
		return s.isAuto();
	}
	public ScheduledExpense getExpense(){
		return s;
	}
}

package definitions;

public class ScheduledExpense {

	private boolean auto;
	private int dueDate;
	private int months;
	private int start;
	private double price;
	private String expense;
	private String payment;
	
	public ScheduledExpense(String expense, double price, int dueDate, String payment, boolean auto, int months, int start){
		this.expense = expense;
		this.price = price;
		this.dueDate = dueDate;
		this.payment = payment;
		this.auto = auto;
		this.months = months;
		this.start = start;
	}
	
	public boolean isAuto(){
		return auto;
	}
	public int getDueDate(){
		return dueDate;
	}
	public int getMonths(){
		return months;
	}
	public int getStart(){
		return start;
	}
	public double getPrice(){
		return price;
	}
	public String getExpense(){
		return expense;
	}
	public String getPayment(){
		return payment;
	}
}

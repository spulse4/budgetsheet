package definitions;

import java.util.ArrayList;
import java.util.List;

public class PriorityQueue {

	private List<Node> queue;
	
	public PriorityQueue(){
		queue = new ArrayList<Node>();
	}
	
	public void add(ScheduledExpense o, int rank){
		Node n = new Node(o, rank);
		for(int i = 0; i < queue.size(); ++i){
			if(o.isAuto() && !queue.get(i).isAuto()){
				continue;
			}
			if(!o.isAuto() && queue.get(i).isAuto()){
				queue.add(i, n);
				return;
			}
			if(queue.get(i).getRank() > rank){
				queue.add(i, n);
				return;
			}
		}
		queue.add(n);
	}
	
	public List<ScheduledExpense> getList(){
		List<ScheduledExpense> expenses = new ArrayList<ScheduledExpense>();
		for(Node n : queue){
			expenses.add(n.getExpense());
		}
		return expenses;
	}	
}

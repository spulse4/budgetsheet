package definitions;

import java.util.ArrayList;
import java.util.List;

import exceptions.AccountException;

public enum Account{

	BARNES_AND_NOBLE_CREDIT("Credit", "2211"),
	CASH("Debit"),
	CHASE_DEBIT("Debit", "0604", "3296"),
	CHASE_FREEDOM("Credit", "7103"),
	CHASE_FREEDOM_UNLIMITED("Credit", "2129"),
	COIN("Debit"),
	COSTCO("Credit", "4200", "4226"),
	DISCOVER("Credit", "6187"),
	GIFT_CARD("Debit"),
	GOVERNMENT_LOANS("Debt"),
	M_WF_CREDIT("Credit", "4224"),
	R_US_CREDIT("Credit", "0358"),
	S_WF_CREDIT("Credit", "3572"),
	VS_CREDIT("Credit", "1100"),
	WF_DEBIT("Debit", "7246", "9152"),
	WF_LOAN("Debt"),
	N_A("Debit");
	
	
	private String type;
	private List<String> accountNumbers;
	
	public static Account fromString(String str) throws AccountException{
		if(str.startsWith("Gift Card")){
			return GIFT_CARD;
		}
		if(str.equals("")){
			return N_A;
		}
		str = str.toLowerCase();
		str = str.replaceAll("_", " ");
		String strShort = str;
		while(strShort.length() > 0 && strShort.startsWith("0")){
			strShort = strShort.substring(1);
		}
		for(Account acc : Account.values()){
			String a = acc.toString().toLowerCase();
			a = a.replaceAll("_", " ");
			if(a.equals(str)){
				return acc;
			}
			for(String s : acc.getAccountNumbers()){
				String t = s;
				while(t.length() > 0 && t.startsWith("0")){
					t = t.substring(1);
				}
				if(t.equals(strShort)){
					return acc;
				}
			}
		}
		throw new AccountException();
	}
	
	private Account(String type, String... numbers){
		this.type = type;
		accountNumbers = new ArrayList<String>();
		for(String str : numbers){
			accountNumbers.add(str);
		}
	}
	
	public String getType(){
		return type;
	}
	
	public List<String> getAccountNumbers(){
		return accountNumbers;
	}
	
	public String getString(){
		String temp = this.toString().toLowerCase();
		StringBuilder str = new StringBuilder();
		boolean space = true;
		for(int i = 0; i < temp.length(); ++i){
			if(space){
				str.append(Character.toUpperCase(temp.charAt(i)));
				space = false;
			}
			else{
				if(temp.charAt(i) == '_'){
					str.append(" ");
					space = true;
				}
				else{
					str.append(temp.charAt(i));
				}
			}
		}
		temp = str.toString();
		temp = temp.replaceAll("Wf", "WF");
		temp = temp.replaceAll("Vs", "VS");
		return temp;
	}
	
	public String getAbbreviated(){
		if(this == Account.N_A){
			return "N/A";
		}
		String temp = getString();
		temp = temp.replaceAll("Barnes And Noble", "B&N");
		temp = temp.replaceAll("Chase", "C");
		temp = temp.replaceAll("Freedom Unlimited", "F Unlimited");
		temp = temp.replaceAll("Government Loans", "FAFSA");
		return temp;
	}
}

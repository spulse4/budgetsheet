package definitions;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Expense implements Comparable<Expense>{

	private long date;
	private String store;
	private Category category;
	private String description;
	private Account account;
	private double cost;
	
	public Expense(long date, String store, Category category,
				   String description, Account account, double cost){
		this.date = date;
		this.store = store;
		this.category = category;
		this.description = description;
		this.account = account;
		this.cost = cost;
		if(this.store.length() > 15){
			this.store = this.store.substring(0, 10) + "...";
		}
		if(this.description.length() > 15){
			this.description = this.description.substring(0, 10) + "...";
		}
	}
	
	public long getDate(){
		return date;
	}
	public String getDateFormatted(){
		Date d = new Date(date);
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yy");
		return format.format(d);
	}
	public String getStore(){
		return store;
	}
	public Category getCategory(){
		return category;
	}
	public String getDescription(){
		return description;
	}
	public Account getAccount(){
		return account;
	}
	public double getCost(){
		return cost;
	}
	public String getCostFormatted(){
		DecimalFormat df = new DecimalFormat("$#,##0.00");
		return df.format(cost);
	}

	@Override
	public int compareTo(Expense o) {
		return Long.compare(this.date, o.date);
	}
}
